# frozen_string_literal: true

module Seshbot
  module Packing
    # :nodocs:
    class LineItem
      attr_accessor :sku_fragment, :quantity, :price

      def initialize(sku_fragment, quantity, price=0)
        raise 'SKU fragment must be a string' if sku_fragment.class != String
        raise 'Quantity must be an integer' if quantity.class != Integer

        @sku_fragment = sku_fragment
        @quantity = quantity
        @price = price
      end

      def to_s
        "#{quantity}x#{sku_fragment}"
      end

      def self.summarise(items)
        "#{items.map(&:to_s)}"
      end

      def self.total_quantity(items)
        items.map { |i| i.quantity }.sum
      end

      def self.merge_line_items(items)
        result = Hash.new(0)
        # create a hash were the keys are sku_fragment, and the values are the summed quantities
        items.each do |item|
          result[item.sku_fragment] += item.quantity
        end
        result.delete_if { |k,v| v == 0 }
        result.map { |sku_fragment, quantity| LineItem.new(sku_fragment, quantity) }
      end
    end
  end # module Packing
end # module Seshbot
