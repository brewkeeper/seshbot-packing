$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require 'yaml'
require "seshbot/packing"

require "minitest/autorun"

module PackingHelper
  def compare_items(item1, item2)
    item1_ = item1.map { |li| [li.sku_fragment, li.quantity] }
    item2_ = item2.map { |li| [li.sku_fragment, li.quantity] }
    raise "Items not the same #{item1_} / #{item2_}" if item1_.sort != item2_.sort

    true
  end
end

include PackingHelper
