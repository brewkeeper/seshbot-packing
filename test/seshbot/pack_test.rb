require 'test_helper'

module Seshbot
  class PackTest < Minitest::Test
    def setup
      @recipes = YAML.load_file('test/seshbot/recipes.yml')
    end

    # pack method
    def test_packs_from_6_packs_bottles
      # A. 1x 6-pack = 6-pack shipping
      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B306', 1)], packed_item)

      # B. 2x 6-packs = 12-pack shipping
      # C. 1x 12-pack = 12-pack shipping
      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('B312', 1)], packed_item)

      # D. 2x 12-pack = 24-pack shipping
      # E. 1x 24-pack = 24-pack shipping
      # F. 2x 6-packs + 1x 12pack = 24-pack shipping
      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 4)])
      assert_equal true, compare_items([Packing::LineItem.new('B324', 1)], packed_item)

      # G. 3x 6-packs = 18-pack shipping
      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 3)])
      assert_equal true, compare_items([Packing::LineItem.new('B318', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 9)])
      assert_equal true, compare_items([Packing::LineItem.new('B306', 1), Packing::LineItem.new('B324', 2)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 6)])
      assert_equal true, compare_items([Packing::LineItem.new('B312', 1), Packing::LineItem.new('B324', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 5)])
      assert_equal true, compare_items([Packing::LineItem.new('B306', 1), Packing::LineItem.new('B324', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 7)])
      assert_equal true, compare_items([Packing::LineItem.new('B318', 1), Packing::LineItem.new('B324', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 8)])
      assert_equal true, compare_items([Packing::LineItem.new('B324', 2)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('B306', 10)])
      assert_equal true, compare_items([Packing::LineItem.new('B324', 2), Packing::LineItem.new('B312', 1)], packed_item)
    end

    def test_packs_from_6_packs_cans
      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('C306', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('C312', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 3)])
      assert_equal true, compare_items([Packing::LineItem.new('C318', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 4)])
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 5)])
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1), Packing::LineItem.new('C306', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 6)])
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1), Packing::LineItem.new('C312', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 7)])
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1), Packing::LineItem.new('C318', 1)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 8)])
      assert_equal true, compare_items([Packing::LineItem.new('C324', 2)], packed_item)

      packed_item = Packing.pack(@recipes, [Packing::LineItem.new('C306', 9)])
      assert_equal true, compare_items([Packing::LineItem.new('C324', 2), Packing::LineItem.new('C306', 1)], packed_item)
    end
  end
end
