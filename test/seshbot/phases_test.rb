require 'test_helper'
require 'timecop'

module Seshbot
  class PhasesTest < Minitest::Test
    def setup
      @recipes = YAML.load_file('test/seshbot/recipes-phases.yml')
    end

    def test_legacy_behaviour
      Timecop.freeze(DateTime.new(2021, 1, 1))
        # using old recipes - should create 24 can carton and the rest of bottles should go towards a 18-pack of bottles
        recipes = YAML.load_file('test/seshbot/recipes.yml')

        items = [Packing::LineItem.new('C306', 7)]
        bundled_items = Packing::bundle_items(recipes, items)

        assert_equal true, compare_items([Packing::LineItem.new('C324', 1), Packing::LineItem.new('B318', 1)], bundled_items)
      Timecop.return
    end

    def test_legacy_behaviour_ends
      Timecop.freeze(DateTime.parse('2021-04-23T08:01:00+09:00'))
        # using old recipes - should leave everything as cans
        recipes = YAML.load_file('test/seshbot/recipes.yml')

        items = [Packing::LineItem.new('C306', 7)]
        bundled_items = Packing::bundle_items(recipes, items)

        assert_equal true, compare_items([Packing::LineItem.new('C324', 1), Packing::LineItem.new('C318', 1)], bundled_items)
      Timecop.return
    end

    def test_can_cartons_survive
      Timecop.freeze(DateTime.new(2021, 5, 1))
        # 10 + 6 = 16 = 12-pack + 4 singles
        items = [Packing::LineItem.new('C301', 10), Packing::LineItem.new('C306', 1)]
        bundled_items = Packing::bundle_items(@recipes, items)

        assert_equal true, compare_items([Packing::LineItem.new('C312', 1), Packing::LineItem.new('B501', 4)], bundled_items)
      Timecop.return
    end
  end
end
