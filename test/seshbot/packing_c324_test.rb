# NOTE: all tests must begin 'def test_name`
# if test_ is not there, it won't run

require 'test_helper'
require 'timecop'

module Seshbot
  class PackingC324Test < Minitest::Test
    def setup
      @recipes = YAML.load_file('test/seshbot/recipes.yml')
    end

    def test_separating_cans
      items = [Packing::LineItem.new('C306', 4), Packing::LineItem.new('B306', 4)]
      cans = Packing::_filter_by_sku_fragment_prefix(items, 'C')
      assert_equal true, compare_items([Packing::LineItem.new('C306', 4)], cans)

      items = [Packing::LineItem.new('C324', 1), Packing::LineItem.new('B306', 4)]
      cans = Packing::_filter_by_sku_fragment_prefix(items, 'C324')
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1)], cans)
    end

    def test_no_cans
      items = [Packing::LineItem.new('B306', 4), Packing::LineItem.new('B306', 4)]
      cans = Packing::_filter_by_sku_fragment_prefix(items, 'C')
      assert_equal [], cans
    end

    def test_c324s_scenario1
      items = [Packing::LineItem.new('C306', 5), Packing::LineItem.new('B306', 1)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1), Packing::LineItem.new('C306', 1), Packing::LineItem.new('B306', 1)], bundled_items)
    end

    def test_c324s_scenario1_legacy
      Timecop.freeze(DateTime.new(2021, 1, 1))
        items = [Packing::LineItem.new('C306', 5), Packing::LineItem.new('B306', 1)]
        bundled_items = Packing::bundle_items(@recipes, items)
        assert_equal true, compare_items([Packing::LineItem.new('B312', 1), Packing::LineItem.new('C324', 1)], bundled_items)
      Timecop.return
    end

    def test_c324s_scenario2
      items = [Packing::LineItem.new('C312', 1),
               Packing::LineItem.new('C312', 1),
               Packing::LineItem.new('B306', 1),
               Packing::LineItem.new('B306', 1),]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal true, compare_items([Packing::LineItem.new('B312', 1), Packing::LineItem.new('C324', 1)], bundled_items)
    end
  end
end
