# NOTE: all tests must begin 'def test_name`
# if test_ is not there, it won't run

require 'test_helper'

module Seshbot
  class PackingTest < Minitest::Test
    def setup
      @recipes = YAML.load_file('test/seshbot/recipes.yml')
      @recipes_phases = YAML.load_file('test/seshbot/recipes-phases.yml')
    end

    def test_that_it_has_a_version_number
      refute_nil Packing::VERSION
    end

    def test_counts_by_fragment
      items = [Packing::LineItem.new('B306', 2),
               Packing::LineItem.new('B306', 1),
               Packing::LineItem.new('B312', 2),
               Packing::LineItem.new('B312', 2),
               Packing::LineItem.new('KL10', 1)]

      grouped_items = Packing::LineItem::merge_line_items(items)
      assert_equal true, compare_items([Packing::LineItem.new('B306', 3), Packing::LineItem.new('B312', 4), Packing::LineItem.new('KL10', 1)], grouped_items)
    end

    def test_substitute_sku
      items = [Packing::LineItem.new('B306', 2),
               Packing::LineItem.new('B306', 1)]

      new_items = Packing::_substitute_sku(items, /^B/, 'C')
      new_items.each { |i| assert_equal 'C', i.sku_fragment[0] }
    end

    # bundle_items method
    def test_bundled_items
      # A. 1x 6-pack = 6-pack shipping
      items = [Packing::LineItem.new('B306', 1)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B306', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity

      # B. 2x 6-packs = 12-pack shipping
      items = [Packing::LineItem.new('B306', 2)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B312', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity

      # C. 1x 12-pack = 12-pack shipping
      items = [Packing::LineItem.new('B312', 1)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B312', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity

      # D. 2x 12-pack = 24-pack shipping
      items = [Packing::LineItem.new('B312', 2)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B324', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity

      # E. 1x 24-pack = 24-pack shipping
      items = [Packing::LineItem.new('B324', 1)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B324', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity

      # F. 2x 6-packs + 1x 12pack = 24-pack shipping
      items = [Packing::LineItem.new('B306', 2),
               Packing::LineItem.new('B312', 1)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B324', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity

      # G. 3x 6-packs = 18-pack shipping
      items = [Packing::LineItem.new('B306', 3)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B318', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity
    end

    def test_singles_to_six_pack
      items = [Packing::LineItem.new('B301', 6)]
      bundled_items = Packing::bundle_items(@recipes, items)
      assert_equal 1, bundled_items.count
      assert_equal 'B306', bundled_items[0].sku_fragment
      assert_equal 1, bundled_items[0].quantity
    end

    # 3 new scenarios to test inputs and outputs using phases
    def test_scenario1
      items = [Packing::LineItem.new('C301', 12), Packing::LineItem.new('C301', 6), Packing::LineItem.new('C301', 6)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('C324', 1)], bundled_items)
    end

    def test_scenario2
      items = [Packing::LineItem.new('C301', 12), Packing::LineItem.new('C301', 6), Packing::LineItem.new('B301', 6)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('B524', 1)], bundled_items)
    end

    def test_scenario3
      items = [Packing::LineItem.new('C301', 5), Packing::LineItem.new('B501', 11), Packing::LineItem.new('B301', 14)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('B524', 1), Packing::LineItem.new('B506', 1)], bundled_items)
    end

    def test_scenario4
      items = [Packing::LineItem.new('B306', 4)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('B524', 1)], bundled_items)

      items = [Packing::LineItem.new('B306', 3), Packing::LineItem.new('C306', 1)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('B524', 1)], bundled_items)

      items = [Packing::LineItem.new('B306', 2), Packing::LineItem.new('C306', 2)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('B524', 1)], bundled_items)

      items = [Packing::LineItem.new('B306', 1), Packing::LineItem.new('C306', 3)]
      bundled_items = Packing::bundle_items(@recipes_phases, items)
      assert_equal true, compare_items([Packing::LineItem.new('B524', 1)], bundled_items)
    end
  end
end
