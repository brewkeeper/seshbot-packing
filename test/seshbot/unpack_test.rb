require 'test_helper'

module Seshbot
  class UnpackTest < Minitest::Test
    def setup
      @recipes = YAML.load_file('test/seshbot/recipes.yml')
    end

    # unpack method
    def test_unpacks_to_single_bottles
      # A. 1x 6-pack = 6-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B306', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 6)], unpacked_item)

      # B. 2x 6-packs = 12-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B306', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 12)], unpacked_item)

      # C. 1x 12-pack = 12-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B312', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 12)], unpacked_item)

      # D. 2x 12-pack = 24-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B312', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 24)], unpacked_item)

      # E. 1x 24-pack = 24-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B324', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 24)], unpacked_item)

      # F. 2x 6-packs + 1x 12pack = 24-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B324', 1), Packing::LineItem.new('B312', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 36)], unpacked_item)

      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B306', 2), Packing::LineItem.new('B312', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 24)], unpacked_item)

      # G. 3x 6-packs = 24-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B306', 3)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 18)], unpacked_item)

      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B312', 2), Packing::LineItem.new('B324', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 48)], unpacked_item)

      # Should ignore anything that can't be unpackd
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B312', 4), Packing::LineItem.new('B306', 3), Packing::LineItem.new('L10', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 66), Packing::LineItem.new('L10', 1)], unpacked_item)

      # 1x 6-pack + 1x 12pack = 18-pack shipping
      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B306', 1), Packing::LineItem.new('B312', 1)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 18)], unpacked_item)

      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B306', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 12)], unpacked_item)

      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B312', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 24)], unpacked_item)

      unpacked_item = Packing.unpack(@recipes, [Packing::LineItem.new('B324', 2)])
      assert_equal true, compare_items([Packing::LineItem.new('B301', 48)], unpacked_item)
    end
  end
end
