require 'test_helper'

module Seshbot
  class RecipeTest < Minitest::Test
    Recipe = Seshbot::Packing::Recipe
    LineItem = Seshbot::Packing::LineItem

    def setup
      @recipes = YAML.load_file('test/seshbot/recipes.yml')
    end

    def test_find_best_recipe
      # 1x12->2x6
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B312', 1)], unpacking: true)
      assert best_recipe == { 'input_fragment' => 'B312', 'input_quantity' => 1, 'output_fragment' => 'B306',
                              'output_quantity' => 2 }

      # 1x24->4x6
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B324', 1)], unpacking: true)
      assert best_recipe == { 'input_fragment' => 'B324', 'input_quantity' => 1, 'output_fragment' => 'B306',
                              'output_quantity' => 4 }
      # 1x6->1x6
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 1)])
      assert best_recipe.nil?

      # 2x6->1x12
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 2)])
      assert best_recipe == { 'input_fragment' => 'B306', 'input_quantity' => 2, 'output_fragment' => 'B312',
                              'output_quantity' => 1 }

      # 3x6->1x24
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 3)])
      assert best_recipe == { 'input_fragment' => 'B306', 'input_quantity' => 3, 'output_fragment' => 'B318',
                              'output_quantity' => 1 }

      # 4x6->1x24
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 4)])
      assert best_recipe == { 'input_fragment' => 'B306', 'input_quantity' => 4, 'output_fragment' => 'B324',
                              'output_quantity' => 1 }

      # 7x6->1x24
      best_recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 7)])
      assert best_recipe == { 'input_fragment' => 'B306', 'input_quantity' => 4, 'output_fragment' => 'B324',
                              'output_quantity' => 1 }
    end

    # find_recipes method
    def test_should_return_nil_if_no_recipe_exists
      recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 1)])
      assert recipe.nil?
      recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B312', 1)])
      assert recipe.nil?
      recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('KL10', 1)])
      assert recipe.nil?
    end

    # 12 pack recipes
    def test_should_return_12_pack_recipe
      recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 2)])
      assert recipe == { 'input_fragment' => 'B306', 'input_quantity' => 2, 'output_fragment' => 'B312',
                         'output_quantity' => 1 }
    end

    # 18 pack recipes
    def test_should_return_18_pack_recipe
      recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 3)])
      assert recipe == { 'input_fragment' => 'B306', 'input_quantity' => 3, 'output_fragment' => 'B318',
                         'output_quantity' => 1 }
    end

    # 24 pack recipes
    def test_should_return_24_pack_recipe
      recipe = Recipe.find_best_recipe(@recipes, [LineItem.new('B306', 4)])
      assert recipe == { 'input_fragment' => 'B306', 'input_quantity' => 4, 'output_fragment' => 'B324',
                         'output_quantity' => 1 }
    end
  end
end