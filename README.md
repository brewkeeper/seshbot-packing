# Seshbot::Packing

This gem allows inventory to be packed for shipping - used by BrewKeeper for pick/pack logic, and by Carrierbot to aid in calculating 'bundled' shipping rates

## Recipes

The packing rules are described as 'recipes' - one of which is represented as a hash of the format:

```ruby
{
  'input_fragment': 'B306',
  'input_quantity': 1,
  'output_fragment': 'B301',
  'output_quantity': '6',
  'is_reversible': true # optional
}
```

The process of packing a collection of line-items in a customer shipment into a smplified set of line-items representing the most cost effective way to pack those items is as follows:

* get all quantity+SKUs in the order/fulfillments in the request
* unpack as much as possible by:
** reverse all recipes inputs/outputs
** find the ‘best’ recipe (the recipe that gives the MOST outputs)
** repeat until no more unpacking can be done
* pack as much as possible by:
** find the ‘best’ recipe (the recipe that gives the FEWEST outputs)
** repeat until no more packing can be done

Therre area few special considerations:
- optional `is_reversible` flag: some recipes may not deemed reversible for the 'unpacking' phase; e.g., 3xB306->1xB324. These should be marked as 'is_reversible': false
- optional `phase` flag: packing recipes may be performed in 'phases' in order to ensure certain rules are adhered to for specific products or variant types (e.g., pack cans into special can-cartons before allowing the remainders to be mixed in with bottles)

Note that if a recipe is found, _it will be applied_ unless a better rule is found.

## Building

* Update the VERSION file
* `gem build seshbot-packing.gemspec`
* commit changes
* `gem push seshbot-packing-VERSION.gem`

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'seshbot-packing'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install seshbot-packing

## Methods

```ruby
# Bundle Items
## this is the main method, it calls most other methods to get its result
items = [{ 'sku' => 'B-B306', 'quantity' => 2, 'price' => 1 }]
Seshbot::Packing.bundle_items(items)
#=> [{ 'sku' => 'BUND-B312', 'quantity' => 1, 'price' => 0 }]

# Count Product Types
## groups together everythign after the `-` and counts them
items = [{"sku" => "A-B306", "quantity" => 2}, {"sku" => "B-B306", "quantity" => 1}]
Seshbot::Packing.count_product_types(items)
#=> {"B306"=>3}

# Separate SKU
## returns the hashes where the sku value matches the given string
items = [{ 'sku' => 'A-C306', 'quantity' => 4, 'price' => 1 },
         { 'sku' => 'A-B306', 'quantity' => 4, 'price' => 1 }]
Seshbot::Packing._filter_by_sku(items, '-C')
#=> [{ 'sku' => 'A-C306', 'quantity' => 4, 'price' => 1 }]
## also takes a named argument
Seshbot::Packing._filter_by_sku(items, '-C', inverse: true)
#=> [{ 'sku' => 'A-B306', 'quantity' => 4, 'price' => 1 }]

# Substitute SKU
## replaces an sku in a hash
Seshbot::Packing._substitute_sku(items, '-B', '-C')
#=> items = [{"sku" => "A-C306", "quantity" => 2}, {"sku" => "B-C306", "quantity" => 1}]
```


## C324s
As of v0.8.0, C324s will be treated differently. If there are any C324s that can exist, they will stay as C324s, and not converted to bottles.
eg.
In:  1x C312, 1x C312, 1x C306, 1x B306
Out: 1x C324, 1x B312

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/seshbot-packing. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/seshbot-packing/blob/master/CODE_OF_CONDUCT.md).


## Code of Conduct

Everyone interacting in the Seshbot::Packing project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/seshbot-packing/blob/master/CODE_OF_CONDUCT.md).
